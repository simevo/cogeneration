/** @file DeltaT.h
    @brief DeltaT class declaration

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2024 Paolo Greppi simevo s.r.l.
 */

/*====================================================================================================================*/
/*==  Guards, Includes  ==============================================================================================*/
/*====================================================================================================================*/

// INCLUDE GUARDS
//
#ifndef LIBPF_DELTAT_H
#define LIBPF_DELTAT_H

/* SYSTEM/GENERAL PURPOSE INCLUDES */
//

/* LIBPF INCLUDES */
//
#include <libpf/flowsheet/FlowSheet.h>

/* LOCAL INCLUDES */
//

/* FORWARD REFERENCES */
//

/*====================================================================================================================*/
/*==  Class DeltaT declaration =======================================================================================*/
/*====================================================================================================================*/

/// Calculate gas combustion adiabatic temperature change
class DeltaT : public FlowSheet {
private:
  static std::string type_;
public:
  // LIFECYCLE
  DeltaT(Libpf::Persistency::Defaults defaults, uint32_t id=0, Persistency *persistency=nullptr, Persistent *parent=nullptr, Persistent *root=nullptr);

  // VARIABLES
  QUANTITY(maxAdiabaticCombT, "adiabatic combustion temperature with air",  0.0, "K");

  // OPERATIONS
  // mandatory
  void setup(void);
  const std::string &type(void) const { return type_; }
  void makeUserEquations(std::list<Assignment *>::iterator &p);
  void pre(Calculatable::SolutionMode solutionMode, int level) { }
  void post(Calculatable::SolutionMode solutionMode, int level);
  // non-mandatory
}; // class DeltaT

#endif // LIBPF_DELTAT_H
