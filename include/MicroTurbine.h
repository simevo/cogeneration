/** @file MicroTurbine.h
    @brief Definition of the MicroTurbine model class

    @attention This file is part of LIBPF
    @attention All rights reserved; do not distribute without permission.
    @author (C) Copyright 2009-2024 Paolo Greppi simevo s.r.l.
 */

#ifndef LIBPF_MICROTURBINE_H
#define LIBPF_MICROTURBINE_H

/* SYSTEM INCLUDES */

/* PROJECT INCLUDES */
#include <libpf/core/Model.h>
#include <libpf/flowsheet/FlowSheet.h>

/* LOCAL INCLUDES */

/* FORWARD REFERENCES */

/*====================================================================================================================*/
/*==  MicroTurbine class declaration  ================================================================================*/
/*====================================================================================================================*/

/// parameterized micro-turbine power generation unit, can match C30, C65 and C200
class MicroTurbine : public FlowSheet {
private:
  const static std::string type_; ///< keep this public for the alias registration
public:
  MicroTurbine(Libpf::Persistency::Defaults defaults, uint32_t id=0, Persistency *persistency=nullptr, Persistent *parent=nullptr, Persistent *root=nullptr);

  Quantity LHV_CH4;      ///< methane Lower Heating Value
  Quantity LHV_H2;       ///< hydrogen Lower Heating Value
  Quantity LHV_CO;       ///< carbon monoxide Lower Heating Value

  Quantity powerIn;      ///< Lower Heating Value input
  Quantity powerEl;      ///< Total net electric output
  Quantity etaEl;        ///< Overall electric efficiency

  Quantity beta0;        ///< Compressor compression ratio (cr) at the nominal point
  Quantity TC0;          ///< Compressor inlet temperature at the nominal point (S01)
  Quantity PC0;          ///< Compressor inlet pressure at the nominal point (S01)
  Quantity PT0;          ///< Turbine inlet pressure at the nominal point (S03)
  Quantity mC0;          ///< Compressor mass flow at the nominal point
  Quantity mT0;          ///< Turbine mass flow at the nominal point

  Quantity TIT;          ///< should be Turbine Inlet Temperature
  Quantity Ctheta0;      ///< Compressor isentropic yield at the nominal point
  Quantity Ttheta0;      ///< Turbine isentropic yield at the nominal point

  Quantity n0;           ///< Compressor / Turbine frequency at the nominal point
  Quantity n;            ///< Compressor / Turbine frequency
  Quantity nr;           ///< Compressor / Turbine reduced frequency

  String model;          ///< Turbine model, valid values: C30, C65 and C200

  // CUSTOM function

  // MANDATORY
  const std::string &type(void) const { return type_; }
  void makeUserEquations(std::list<Assignment *>::iterator &p);
  void setup(void);
  void pre(SolutionMode solutionMode, int level = 0) { }
  void post(SolutionMode solutionMode, int level = 0);

  // NON-MANDATORY
  int sequential(void) { return 5; }
  bool supportsSimultaneous(void) { return true; }
  int maximumIterations(void) { return 200; }
}; // MicroTurbine

#endif // LIBPF_MICROTURBINE_H
