/** @file Cogeneration.h
 * @brief Contains the interface of the Cogeneration class

 * This file is part of LIBPF
 * All rights reserved; do not distribute without permission.
 * @author (C) Copyright 2010-2024 Paolo Greppi simevo s.r.l., Daniele Bernocco; (C) Copyright 2012-2024 Paolo Greppi simevo s.r.l.
 */

/*====================================================================================================================*/
/*==  Guards, Includes  ==============================================================================================*/
/*====================================================================================================================*/

// INCLUDE GUARDS
//
#ifndef LIBPF_Cogeneration_H
#define LIBPF_Cogeneration_H

/* SYSTEM/GENERAL PURPOSE INCLUDES */
//

/* LIBPF INCLUDES */
//
#include <libpf/flowsheet/FlowSheet.h>

/* LOCAL INCLUDES */
//

/* FORWARD REFERENCES */
//

/*====================================================================================================================*/
/*==  Class Cogeneration declaration =================================================================================*/
/*====================================================================================================================*/

/** @class Cogeneration
 * Combined Heat and Power with configurable turbine
 *
 * #include <Cogeneration.h>
 *
 */
class Cogeneration : public FlowSheet {
private:
  static std::string type_;
public:
  Cogeneration(Libpf::Persistency::Defaults defaults, uint32_t id=0, Persistency *persistency=nullptr, Persistent *parent=nullptr, Persistent *root=nullptr);

  // input:
  QUANTITY(energyPriceTh, "Thermal energy price",  4.025, "$/mmBTU");
  QUANTITY(TwaterIn, "Inlet water temperature", 60.0 + 273.15, "K");
  QUANTITY(TwaterOut, "Outlet water temperature", 80.0 + 273.15, "K");

  // output:
  QUANTITY(Tfumes, "Outlet temperature of the fumes", 80.0 + 273.15, "K");
  QUANTITY(energyPriceEl, "Electrical energy price",  0.0, "$/mmBTU");
  QUANTITY(powerIn, "Power input on Lower Heating Value basis", 100.0, "kW");
  QUANTITY(We, "Electrical power output", 30.0, "kW");
  QUANTITY(Wt, "Thermal power output", 30.0, "kW");
  QUANTITY(W, "Overall power output", 60.0, "W");
  QUANTITY(etaE, "Electrical yield", 0.4, "");
  QUANTITY(etaT, "Thermal yield", 0.30, "");
  QUANTITY(eta, "Overall yield", 0.6, "");
  QUANTITY(mdot, "Hot water mass flow", 0.0, "kg/h");

  /// Turbine model, valid values: C30, C65, C200 and C1000
  STRING(turbine, "Turbine model", "C30");

  // CUSTOM function

  // MANDATORY
  const std::string &type(void) const { return type_; }
  void makeUserEquations(std::list<Assignment *>::iterator &p);
  void setup(void);
  void pre(SolutionMode solutionMode, int level = 0) { }
  void post(SolutionMode solutionMode, int level = 0);

  // NON-MANDATORY
};  // Cogeneration

#endif // LIBPF_Cogeneration_H
