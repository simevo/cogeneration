/** @file Burner.h
    @brief Burner class declaration

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2024 Paolo Greppi simevo s.r.l.
 */

/*====================================================================================================================*/
/*==  Guards, Includes  ==============================================================================================*/
/*====================================================================================================================*/

// INCLUDE GUARDS
//
#ifndef LIBPF_BURNER_H
#define LIBPF_BURNER_H

/* SYSTEM/GENERAL PURPOSE INCLUDES */
//

/* LIBPF INCLUDES */
//
#include <libpf/units/FlashDrum.h>

/* LOCAL INCLUDES */
//

/* FORWARD REFERENCES */
//

/*====================================================================================================================*/
/*==  Class Burner declaration =======================================================================================*/
/*====================================================================================================================*/

/// Combustion of idrocarbons, hydrogen and carbon monoxide
/// @remark the default conversions are zero
class Burner : public FlashDrum {
private:
  static std::string type_;
public:
  Burner(Libpf::Persistency::Defaults defaults, uint32_t id=0, Persistency *persistency=nullptr, Persistent *parent=nullptr, Persistent *root=nullptr);
  /// @returns the lower heating value of the supplied phase Object
  /// @param[in] s the Total phase of a stream
  static Value LHV(const Object &s);
}; // class Burner

#endif // LIBPF_BURNER_H
