/** @file DeltaH.h
    @brief DeltaH class declaration

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2024 Paolo Greppi simevo s.r.l.
*/

/*====================================================================================================================*/
/*==  Guards, Includes  ==============================================================================================*/
/*====================================================================================================================*/

// INCLUDE GUARDS
//
#ifndef LIBPF_DELTAH_H
#define LIBPF_DELTAH_H

/* SYSTEM/GENERAL PURPOSE INCLUDES */
//

/* LIBPF INCLUDES */
//
#include <libpf/flowsheet/FlowSheet.h>

/* LOCAL INCLUDES */
//

/* FORWARD REFERENCES */
//
class Assignment;

/*====================================================================================================================*/
/*==  Class DeltaH  declaration ======================================================================================*/
/*====================================================================================================================*/

class DeltaH : public FlowSheet {
private:
  static std::string type_;
public:
  DeltaH(Libpf::Persistency::Defaults defaults, uint32_t id=0, Persistency *persistency=nullptr, Persistent *parent=nullptr, Persistent *root=nullptr);

  QUANTITY(lhv, "Lower Heat of Combustion mass base", 0.0, "J/kg");
  QUANTITY(LHV, "Lower Heat of Combustion molar base", 0.0, "J/kmol");

  void makeUserEquations(std::list<Assignment *>::iterator &/* p */) { }
  void setup(void);
  void pre(Calculatable::SolutionMode solutionMode, int level) { }
  void post(Calculatable::SolutionMode solutionMode, int level);
  const std::string &type(void) const {
    return type_;
  }
}; // DeltaH

#endif // LIBPF_DELTAH_H
