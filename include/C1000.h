/** @file C1000.h
    @brief Definition of the C1000 model class

    @attention This file is part of LIBPF
    @attention All rights reserved; do not distribute without permission.
    @author (C) Copyright 2009-2024 Paolo Greppi simevo s.r.l.
 */

#ifndef LIBPF_C1000_H
#define LIBPF_C1000_H

/* SYSTEM INCLUDES */

/* PROJECT INCLUDES */
#include <libpf/flowsheet/FlowSheet.h>

/* LOCAL INCLUDES */

/* FORWARD REFERENCES */

/*====================================================================================================================*/
/*==  C1000 class declaration  =======================================================================================*/
/*====================================================================================================================*/

/// micro-turbine power generation unit, assembles four C200 units to match C1000
class C1000 : public FlowSheet {
private:
  const static std::string type_; ///< keep this public for the alias registration
public:

  C1000(Libpf::Persistency::Defaults defaults, uint32_t id=0, Persistency *persistency=nullptr, Persistent *parent=nullptr, Persistent *root=nullptr);

  QUANTITY(LHV_CH4, "methane Lower Heating Value", 802618.0, "kJ/kmol");
  QUANTITY(LHV_H2, "hydrogen Lower Heating Value", 241814.0, "kJ/kmol");
  QUANTITY(LHV_CO, "carbon monoxide Lower Heating Value", 282980.0, "kJ/kmol");
  QUANTITY(powerIn, "Lower Heating Value input", 0.0, "W");
  QUANTITY(powerEl, "Total net electric output", 0.0, "W");

  // CUSTOM function

  // MANDATORY
  const std::string &type(void) const { return type_; }
  void makeUserEquations(std::list<Assignment *>::iterator &p) { }
  void setup(void);
  void pre(SolutionMode solutionMode, int level = 0) { }
  void post(SolutionMode solutionMode, int level = 0);

  // NON-MANDATORY
}; // C1000

#endif // LIBPF_C1000_H
