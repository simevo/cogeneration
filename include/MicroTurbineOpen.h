/** @file MicroTurbine.h
    @brief Definition of the MicroTurbine model class

    @attention This file is part of LIBPF
    @attention All rights reserved; do not distribute without permission.
    @author (C) Copyright 2009-2024 Paolo Greppi simevo s.r.l.
 */

#ifndef LIBPF_MICROTURBINEOPEN_H
#define LIBPF_MICROTURBINEOPEN_H

/* SYSTEM INCLUDES */

/* PROJECT INCLUDES */
#include <libpf/flowsheet/FlowSheet.h>

/* LOCAL INCLUDES */

/* FORWARD REFERENCES */

/*====================================================================================================================*/
/*==  MicroTurbineOpen class declaration  ================================================================================*/
/*====================================================================================================================*/

/// as MicroTurbine but with hot air inlet and compressed air outlet
class MicroTurbineOpen : public FlowSheet {
private:
  static std::string type_;
public:
  MicroTurbineOpen(Libpf::Persistency::Defaults defaults, uint32_t id=0, Persistency *persistency=nullptr, Persistent *parent=nullptr, Persistent *root=nullptr);

  QUANTITY(LHV_CH4, "methane Lower Heating Value", 802618.0, "kJ/kmol");
  QUANTITY(LHV_H2, "hydrogen Lower Heating Value", 241814.0, "kJ/kmol");
  QUANTITY(LHV_CO, "carbon monoxide Lower Heating Value", 282980.0, "kJ/kmol");

  QUANTITY(powerIn, "Lower Heating Value input", 0.0, "W");
  QUANTITY(power, "Gas Turbine Subsystem power output", 0.0, "W");
  QUANTITY(powerEl, "Total net electric output", 0.0, "W");
  QUANTITY(etaEl, "Overall electric efficiency", 0.0, "");

  QUANTITY(beta0, "Compressor compression ratio at the nominal point", 3.6, "");
  QUANTITY(mC0, "Compressor mass flow at the nominal point", 1.66667, "kg/s");
  QUANTITY(TC0, "Compressor inlet temperature at the nominal point", 296.15, "K");
  QUANTITY(PC0, "Compressor inlet pressure at the nominal point", 101325., "Pa");
  QUANTITY(mrC0, "Compressor corrected mass flow at the nominal point", 0.0, "m*s");
  QUANTITY(mT0, "turbine mass flow at the nominal point", 1.87407, "kg/s");
  QUANTITY(PT0, "turbine inlet pressure at the nominal point", 347676., "Pa");
  QUANTITY(mrT0, "turbine corrected mass flow at the nominal point", 0.0, "m*s");

  QUANTITY(TIT, "should be Turbine Inlet Temperature", t0, "K");
  QUANTITY(mC, "should-be Compressor uncorrected mass flow", 0.0, "kg/s");
  QUANTITY(mT, "should-be turbine uncorrected mass flow", 0.0, "kg/s");
  QUANTITY(maC, "actual Compressor uncorrected mass flow", 0.0, "kg/s");
  QUANTITY(maT, "actual turbine uncorrected mass flow", 0.0, "kg/s");

  QUANTITY(nr, "Compressor and turbine reduced frequency", 0.0, "");

  void makeUserEquations(std::list<Assignment *>::iterator &p);
  void setup(void);
  void pre(SolutionMode solutionMode, int level = 0) { }
  void post(SolutionMode solutionMode, int level = 0);
  const std::string &type(void) const {
    return type_;
  }

  int sequential(void) { return 5; }
  bool supportsSimultaneous(void) { return true; }
  int maximumIterations(void) { return 200; }
}; // MicroTurbineOpen

#endif // LIBPF_MICROTURBINEOPEN_H
