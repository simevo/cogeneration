/** @file CogenerationICE.h
 * @brief Contains the interface of the CogenerationICE class

 * This file is part of LIBPF
 * All rights reserved; do not distribute without permission.
 * @author (C) Copyright 2010-2024 Paolo Greppi simevo s.r.l., Daniele Bernocco; (C) Copyright 2012-2024 Paolo Greppi simevo s.r.l.
 */

/*====================================================================================================================*/
/*==  Guards, Includes  ==============================================================================================*/
/*====================================================================================================================*/

// INCLUDE GUARDS
//
#ifndef LIBPF_COGENERATIONICE_H
#define LIBPF_COGENERATIONICE_H

/* SYSTEM/GENERAL PURPOSE INCLUDES */
//

/* LIBPF INCLUDES */
//
#include <libpf/flowsheet/FlowSheet.h>

/* LOCAL INCLUDES */
//

/* FORWARD REFERENCES */
//

/*====================================================================================================================*/
/*==  Class CogenerationICE declaration ==============================================================================*/
/*====================================================================================================================*/

/** @class CogenerationICE
 * Internal Combustion Engine
 *
 * #include <CogenerationICE.h>
 *
 */
class CogenerationICE : public FlowSheet {
private:
  static std::string type_;
public:
  CogenerationICE(Libpf::Persistency::Defaults defaults, uint32_t id=0, Persistency *persistency=nullptr, Persistent *parent=nullptr, Persistent *root=nullptr);

  // input:
  Quantity energyPriceTh; ///< Thermal energy price
  Quantity etaE; ///< Electrical yield
  Quantity Tfumes; ///< Outlet temperature of the fumes
  Quantity TwaterIn; ///< Inlet hot water temperature
  Quantity TwaterOut; ///< Outlet hot water temperature
  // output:
  Quantity energyPriceEl; ///< Electrical energy price
  Quantity powerIn; ///< Power input on Lower Heating Value basis
  Quantity We; ///< Electrical power output
  Quantity Wt; ///< Thermal power output
  Quantity W; ///< Overall power output
  Quantity etaT; ///< Thermal yield
  Quantity eta; ///< Overall yield
  Quantity mdot; ///< Hot water mass flow
  Quantity AFR; ///< Air to Fuel Ratio

  // CUSTOM function

  // MANDATORY
  const std::string &type(void) const { return type_; }
  void makeUserEquations(std::list<Assignment *>::iterator &p);
  void setup(void);
  void pre(SolutionMode solutionMode, int level = 0) { }
  void post(SolutionMode solutionMode, int level = 0);

  // NON-MANDATORY
};  // CogenerationICE

#endif // LIBPF_COGENERATIONICE_H
