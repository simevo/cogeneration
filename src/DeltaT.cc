/** @file DeltaT.h
@brief DeltaT class implementation

This file is part of LIBPF
All rights reserved; do not distribute without permission.
@author (C) Copyright 2012-2024 Paolo Greppi simevo s.r.l.
*/

/* SYSTEM/GENERAL PURPOSE INCLUDES */
//

/* LIBPF INCLUDES */
//
#include <libpf/utility/diagnostic.h>
#include <libpf/streams/Stream.h>
#include <libpf/components/ListComponents.h> // for components and NCOMPONENTS

/* LOCAL INCLUDES */
//
#include <DeltaT.h>

/* FORWARD REFERENCES */
//

/* MACROS */
// avoid macros!!
//

/* CONSTANTS */
//

/* VARIABLES */
//
static const int verbosityFile = 0;

/* FUNCTIONS */
//

/*====================================================================================================================*/
/*==  Class DeltaT implementation  ===================================================================================*/
/*====================================================================================================================*/

std::string DeltaT::type_("DeltaT");

DeltaT::DeltaT(Libpf::Persistency::Defaults defaults, uint32_t id, Persistency *persistency, Persistent *parent, Persistent *root) :
  Model(defaults, id, persistency, parent, root),
  VertexBase(defaults, id, persistency, parent, root),
  FlowSheet(defaults, id, persistency, parent, root) {
  addVariable(maxAdiabaticCombT);

  if (!persistency) {
    diagnostic(2, "Define unit operations");
    addUnit("Burner", defaults.relay("IdealCombustor", "Ideal Combustion Reactor"));

    diagnostic(2, "Define stream objects and connect");
    addStream("StreamVapor", defaults.relay( "Fuel",  "Fuel"),  "source",  "out",  "IdealCombustor",  "in");
    addStream("StreamVapor", defaults.relay( "Oxydant",  "Oxidant"),  "source",  "out",  "IdealCombustor",  "in");
    addStream("StreamVapor", defaults.relay( "Flue",  "Flue gases"),  "IdealCombustor",  "out",  "sink",  "in");
  }
} // DeltaT::DeltaT

void DeltaT::setup(void) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, " entered for " << tag());

    diagnostic(3, "Calling FlowSheet::setup to initialize any embedded FlowSheet");
    FlowSheet::setup();

    diagnostic(3, "Setting input variables");
    // fuel
    S("Fuel.flashoption") = "PT";
    S("Fuel.flowoption") = "Nx";
    my_cast<Stream *>(&at("Fuel"), CURRENT_FUNCTION)->clearComposition();
    Q("Fuel:Tphase.ndot").set(1.0, "kmol/s");
    if (components.exists("C2H6")) {
      Q("Fuel:Tphase.x", "CH4").set(0.886279381413047);
      Q("Fuel:Tphase.x", "C2H6").set(0.0733813934835132);
      Q("Fuel:Tphase.x", "CO2").set(0.00426476773644518);
      Q("Fuel:Tphase.x", "N2").set(0.036074457366995);
    } else {
      Q("Fuel:Tphase.x", "CH4").set(0.98);
      Q("Fuel:Tphase.x", "CO").set(0.0);
      Q("Fuel:Tphase.x", "H2").set(0.0);
      Q("Fuel:Tphase.x", "N2").set(0.02);
    }

    // oxidant
    Q("Oxydant.T").set(T0);
    Q("Oxydant.P").set(P0);
    S("Oxydant.flashoption") = "PT";
    S("Oxydant.flowoption") = "Nx";
    my_cast<Stream *>(&at("Oxydant"), CURRENT_FUNCTION)->clearComposition();
    Q("Oxydant:Tphase.ndot").set(100.0, "kmol/s");
    Q("Oxydant:Tphase.x", "H2O").set(0.0235);
    Q("Oxydant:Tphase.x", "O2").set(0.2045);
    Q("Oxydant:Tphase.x", "N2").set(0.7720);

    // combustor
    Q("IdealCombustor.deltaP").set(0.0, "mbar");
    Q("IdealCombustor.duty").set(0.0, "W");
    S("IdealCombustor.option") = "DH";
    for (int i = 0; i < I("IdealCombustor.nReactions"); ++i)
      at("IdealCombustor:reactions", i).Q("z").set(1.0);
    maxAdiabaticCombT.setOutput();
  } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // DeltaT::setup

void DeltaT::post(SolutionMode solutionMode, int level) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, "Entered for " << tag());
    maxAdiabaticCombT = Q("IdealCombustor.T");
    diagnostic(3, "Done");
  } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // DeltaT::post

void DeltaT::makeUserEquations(std::list<Assignment *>::iterator &p) {
  static const int verbosityLocal = -1;
  try {
    diagnostic(2, " entered for " << tag());

    int i(0);
    double o2res(0.0244);
    MAKEASSIGNMENT(Q("Oxydant:Tphase.ndot"),
                   (Q("Oxydant:Tphase.ndotcomps", "O2") -
                    (Q("Flue:Tphase.ndotcomps", "O2") - Value(o2res) * (Q("Flue:Tphase.ndot") - Q("Flue:Tphase.ndotcomps", "O2")) / Value(one - o2res))) / Q("Oxydant:Tphase.x", "O2"),
                   "Target residual oxygen in flue gas");
  } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // DeltaT::makeUserEquations
