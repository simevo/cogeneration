/** @file CogenerationDriver.cc
    @brief implement kernel API for Cogeneration demo

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2009-2024 Paolo Greppi simevo s.r.l.
 */

/* SYSTEM/GENERAL PURPOSE INCLUDES */
//
#define _CRT_SECURE_NO_WARNINGS
#include <string>

/* LIBPF INCLUDES */
//
#include <libpf/utility/version.h>
#include <libpf/components/components.h>
#include <libpf/components/ComponentBiochem.h>
#include <libpf/persistency/NodeFactory.h>
#include <libpf/core/Libpf.h> // for initializeLibpf / uninitializeLibpf
#include <libpf/flowsheet/flowsheets.h> // register all flowsheet types with the model factory
#include <libpf/units/units.h> // register all unit operation types with the model factory
#include <libpf/streams/streams.h> // register all stream types with the model factory
#include <libpf/phases/phases.h> // register all phase types with the model factory
#include <libpf/units/reactions.h> // register all reaction types with the model factory
#include <libpf/components/ListComponents.h>       // for NCOMPONENTS
#include <libpf/user/main.h> // for the main function

/* LOCAL INCLUDES */
//
#include <libpf/user/Kernel.h>
// SNIPPET 001 start
// include specific headers
#include "MicroTurbine.h"
#include "MicroTurbineC30.h"
#include "MicroTurbineOpen.h"
#include "C1000.h"
#include "Burner.h"
#include "DeltaH.h"
#include "DeltaT.h"
#include <CogenerationICE.h>
#include <Cogeneration.h>
// SNIPPET 001 end

/* FORWARD REFERENCES */
//

/* MACROS */
// avoid macros!!
//

/* GLOBAL CONSTANTS */
//

/* GLOBAL VARIABLES */
//

/* FUNCTIONS */
//

// SNIPPET 002 start
// register the service
Libpf::User::KernelImplementation impl_("Cogeneration", // name
  "Process modeling demo for Combined Heat and Power plants", // description
  "(C) Copyright 2009-2024 Paolo Greppi simevo s.r.l.", // license
  "0.1.5 [Tue Apr 30 10:50:37 2024 +0200]", // version
  "C1000", // defaultType
  "e688e78c-c36f-48be-ab96-35fadfdc251a" // uuid
);
// SNIPPET 002 end

void Libpf::User::initializeKernel(void) {
  // initialize library
  if (initializeLibpf()) {

    NodeFactory nodeFactory;
// SNIPPET 003 start
    // register the provided types
    nodeFactory.registerType<MicroTurbine>("MicroTurbine", "parameterized micro-turbine power generation unit", "flowsheet", true, { }, { Libpf::Persistency::StringOption("model", "turbine model", "C30", "turbineType") }, { }, "Microturbine", 80.0, 80.0);
    nodeFactory.registerType<MicroTurbineC30>("MicroTurbineC30", "30 kW micro-turbine power generation unit matching C30", "flowsheet", true, { }, { }, { }, "MicroTurbineC30", 80.0, 80.0);
    nodeFactory.registerType<MicroTurbineOpen>("MicroTurbineOpen", "as MicroTurbineC30 but with hot air inlet and compressed air outlet", "flowsheet", true, { }, { }, { }, "MicroTurbineOpen", 80.0, 80.0);
    nodeFactory.registerType<C1000>("C1000", "1 MW micro-turbine power generation unit matching C1000", "flowsheet", true, { }, { }, { }, "C1000", 80.0, 80.0);

    nodeFactory.registerAlias("MicroTurbine", "C30", "30 kW micro-turbine power generation unit matching C30", Libpf::Persistency::Defaults()("model", "C30"), "C30", 80.0, 80.0);
    nodeFactory.registerAlias("MicroTurbine", "C65", "65 kW micro-turbine power generation unit matching C65", Libpf::Persistency::Defaults()("model", "C65"), "C65", 80.0, 80.0);
    nodeFactory.registerAlias("MicroTurbine", "C200", "200 kW micro-turbine power generation unit matching C200", Libpf::Persistency::Defaults()("model", "C200"), "C200", 80.0, 80.0);

    nodeFactory.registerType<Burner>("Burner", "Complete combustion of idrocarbons, hydrogen and carbon monoxide", "unit", false, { }, { }, { }, "Burner", 80.0, 80.0);
    nodeFactory.registerType<DeltaT>("DeltaT", "Compute the adiabatic combustion temperature of a fuel", "flowsheet", true, { }, { }, { }, "DeltaT", 80.0, 80.0);
    nodeFactory.registerType<DeltaH>("DeltaH", "Compute the lower heat of combustion of a fuel", "flowsheet", true, { }, { }, { }, "DeltaH", 80.0, 80.0);
    nodeFactory.registerType<CogenerationICE>("CogenerationICE", "Cogeneration with Internal Combustion Engine", "flowsheet", true, { }, { }, { }, "InternalCombustionEngine", 80.0, 80.0);
    nodeFactory.registerType<Cogeneration>("Cogeneration", "Cogeneration with configurable micro-turbine", "flowsheet", true, { }, { Libpf::Persistency::StringOption("turbine", "Turbine model", "C30", "turbineType") }, { }, "Cogeneration", 80.0, 80.0);
    nodeFactory.registerAlias("Cogeneration", "CogenerationC30", "CHP with C30 turbine", Libpf::Persistency::Defaults()("turbine", "C30"), "CogenerationC30", 80.0, 80.0);
    nodeFactory.registerAlias("Cogeneration", "CogenerationC65", "CHP with C65 turbine", Libpf::Persistency::Defaults()("turbine", "C65"), "CogenerationC65", 80.0, 80.0);
    nodeFactory.registerAlias("Cogeneration", "CogenerationC200", "CHP with C200 turbine", Libpf::Persistency::Defaults()("turbine", "C200"), "CogenerationC200", 80.0, 80.0);
    nodeFactory.registerAlias("Cogeneration", "CogenerationC1000", "CHP with C1000 turbine", Libpf::Persistency::Defaults()("turbine", "C1000"), "CogenerationC1000", 80.0, 80.0);
// SNIPPET 003 end

    components.clear();
// SNIPPET 004 start
    // populate the list of components
    components.addcomp(new purecomps::water);
    components.addcomp(new purecomps::N2);
    components.addcomp(new purecomps::O2);
    components.addcomp(new purecomps::methane);
    components.addcomp(new purecomps::CO);
    components.addcomp(new purecomps::CO2);
    components.addcomp(new purecomps::H2);
// SNIPPET 004 end

    // populate locales
    Libpf::User::addLocale("en");
    
// SNIPPET 005 start
    // populate enumerators
    // supported values: C30, C65, C200 and C1000 (default)
    Libpf::Core::Enumerator turbineType("turbineType", "Turbine type");
    turbineType.addOption(Libpf::Utility::Option("C30", "30 kW micro-turbine power generation unit matching C30"));
    turbineType.addOption(Libpf::Utility::Option("C65", "65 kW micro-turbine power generation unit matching C65"));
    turbineType.addOption(Libpf::Utility::Option("C200", "200 kW micro-turbine power generation unit matching C200"));
    turbineType.addOption(Libpf::Utility::Option("C1000", "(default) 1 MW micro - turbine power generation unit matching C1000"));
    Libpf::User::addEnumerator(turbineType);
// SNIPPET 005 end
  } // the library has actually been initialized
} // initializeKernel

void Libpf::User::uninitializeKernel(void) {
  if (uninitializeLibpf()) {
    components.clear();
  } // the library has actually been uninitialized
} // uninitializeKernel

int main(int argc, char *argv []) {
  return Libpf::User::main(argc, argv);
}
