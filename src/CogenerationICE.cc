/** @file CogenerationICE.cc
 * @brief Contains the implementation of the CogenerationICE class
 *
 * @attention All rights reserved; do not distribute without permission.
 * @author (C) Copyright 2010-2024 Paolo Greppi simevo s.r.l., Daniele Bernocco; (C) Copyright 2012-2024 Paolo Greppi simevo s.r.l.
 */

/* SYSTEM/GENERAL PURPOSE INCLUDES */
//

/* LIBPF INCLUDES */
//
#include <libpf/utility/diagnostic.h> // for diagnostic
#include <libpf/components/ListComponents.h> // for components and NCOMPONENTS
#include <libpf/streams/Stream.h>

/* LOCAL INCLUDES */
//
#include <CogenerationICE.h>
#include <Burner.h>

/* FORWARD REFERENCES */
//

/* MACROS */
// avoid macros!!
//

/* CONSTANTS */
//

/* VARIABLES */
//
static const int verbosityFile = -1;

/* FUNCTIONS */
//

/*====================================================================================================================*/
/*==  Class CogenerationICE implementation  ==========================================================================*/
/*====================================================================================================================*/

std::string CogenerationICE::type_("CogenerationICE");

CogenerationICE::CogenerationICE(Libpf::Persistency::Defaults defaults, uint32_t id, Persistency *persistency, Persistent *parent, Persistent *root) :
  Model(defaults, id, persistency, parent, root),
  VertexBase(defaults, id, persistency, parent, root),
  FlowSheet(defaults, id, persistency, parent, root),
  DEFINE(energyPriceTh, "Thermal energy price",  4.025, "$/mmBTU"),
  DEFINE(etaE, "Electrical yield",  0.4, ""),
  DEFINE(Tfumes, "Outlet temperature of the fumes", 80.0 + 273.15, "K"),
  DEFINE(TwaterIn, "Inlet water temperature", 60.0 + 273.15, "K"),
  DEFINE(TwaterOut, "Outlet water temperature", 80.0 + 273.15, "K"),
  DEFINE(energyPriceEl, "Electrical energy price",  0.0, "$/mmBTU"),
  DEFINE(powerIn, "Power input on Lower Heating Value basis", 100.0, "kW"),
  DEFINE(We, "Electrical power output", 30.0, "kW"),
  DEFINE(Wt, "Thermal power output", 30.0, "kW"),
  DEFINE(W, "Overall power output", 60.0, "W"),
  DEFINE(etaT, "Thermal yield", 0.30, ""),
  DEFINE(eta, "Overall yield", 0.6, ""),
  DEFINE(mdot, "Hot water mass flow", 0.0, "kg/h"),
  DEFINE(AFR, "Air to Fuel Ratio", 1.0, "") {
  static const int verbosityLocal = 0;
  try {
    addVariable(energyPriceTh);
    addVariable(etaE);
    addVariable(Tfumes);
    addVariable(TwaterIn);
    addVariable(TwaterOut);
    addVariable(energyPriceEl);
    addVariable(powerIn);
    addVariable(We);
    addVariable(Wt);
    addVariable(W);
    addVariable(etaT);
    addVariable(eta);
    addVariable(mdot);
    addVariable(AFR);
    if (!persistency) {
      diagnostic(2, "Define unit operations");
      addUnit("Burner", defaults.relay("ICE", "Internal Combustion Engine"));
      addUnit("FlashDrum", defaults.relay("Cogenerator", "Cogeneration heat exchanger"));

      diagnostic(2, "Define stream objects and connect");
      addStream("StreamVapor", defaults.relay("S01", "Air"), "source", "out", "ICE", "in");
      addStream("StreamVapor", defaults.relay("S02", "Fuel"), "source", "out", "ICE", "in");
      addStream("StreamVapor", defaults.relay("S03", "Hot gases"), "ICE", "out", "Cogenerator", "in");
      addStream("StreamVapor", defaults.relay("S04", "Fumes"), "Cogenerator", "out", "sink", "in");
    }
  } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
};  // CogenerationICE::CogenerationICE

void CogenerationICE::post(SolutionMode solutionMode, int level) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, "Entered for " << fullTag());
    energyPriceEl = energyPriceTh / etaE;
    powerIn = Burner::LHV(at("S02:Tphase"));
    We = Q("ICE.duty");
    Wt = Q("Cogenerator.duty");
    W = We + Wt;
    etaT = Wt / powerIn;
    eta = etaE + etaT;
    mdot = components["H2O"]->MW() * Wt / ((components["H2O"]->cpl(TwaterOut, P0) + components["H2O"]->cpl(TwaterIn, P0)) / 2.0) / (TwaterOut - TwaterIn);
    AFR = Q("S01:Tphase.mdot") / Q("S02:Tphase.mdot");
    if (TwaterIn > Tfumes)
      setWarning("potential temperature crossing between specified TwaterIn and Tfumes");
    if (TwaterIn >= TwaterOut)
      setError("TwaterIn sould be lower than TwaterOut");
    diagnostic(3, "Done");
  } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // CogenerationICE::post

void CogenerationICE::setup(void) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, "Entered for " << fullTag());

    // set up feed streams; if not standalone, these values will be overwritten 
    // Air
    Q("S01.T").set(15.0 + 273.15, "K");
    Q("S01.P").set(1.0, "atm");
    S("S01.flowoption") = "Mx";
    my_cast<Stream *>(&at("S01"), CURRENT_FUNCTION)->clearComposition();
    Q("S01:Tphase.mdot").set(30.0 * 8.44136, "kg/h");
    Q("S01:Tphase.x", "N2").set(0.792);
    Q("S01:Tphase.x", "O2").set(0.198);
    Q("S01:Tphase.x", "H2O").set(0.01);

    // Fuel
    Q("S02.T").set(15.0 + 273.15, "K");
    Q("S02.P").set(P0);
    S("S02.flowoption") = "Mx";
    my_cast<Stream *>(&at("S02"), CURRENT_FUNCTION)->clearComposition();
    Q("S02:Tphase.mdot").set(8.44136, "kg/h");
    Q("S02:Tphase.x", "CH4").set(0.98);
    Q("S02:Tphase.x", "CO").set(0.0);
    Q("S02:Tphase.x", "H2").set(0.0);
    Q("S02:Tphase.x", "N2").set(0.02);

    S("ICE.option") = "PT";
    Q("ICE.P").set(P0);
    Q("ICE.T").set(800.0 + 273.15, "K");
    for (int i = 0; i < I("ICE.nReactions"); ++i) {
      at("ICE:reactions", i).Q("z").set(1.0);
      at("ICE:reactions", i).Q("conv").setOutput();
      at("ICE:reactions", i).Q("z").setOutput();
    }

    S("Cogenerator.option") = "PT";
    Q("Cogenerator.P").set(P0);
    Q("Cogenerator.T").set(80.0 + 273.15, "K");

    energyPriceTh.setInput();
    etaE.setInput();
    Tfumes.setInput();
    TwaterIn.setInput();
    TwaterOut.setInput();

    energyPriceEl.setOutput();
    powerIn.setOutput();
    We.setOutput();
    Wt.setOutput();
    W.setOutput();
    etaT.setOutput();
    eta.setOutput();
    mdot.setOutput();
    AFR.setOutput();
    diagnostic(3, "Done");
    } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // CogenerationICE::setup

void CogenerationICE::makeUserEquations(std::list<Assignment *>::iterator &p) {
  static const int verbosityLocal = -1;
  try {
    diagnostic(2, "Entered for " << tag());
    int i(0);
    MAKEASSIGNMENT(Q("ICE.T"), Q("ICE.T") * pow(powerIn * etaE / We, 0.25), "Set outlet temperature of the Internal Combustion Engine");
    MAKEASSIGNMENT(Q("Cogenerator.T"), Tfumes, "Set outlet temperature of the fumes");
  } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // CogenerationICE::makeUserEquations
