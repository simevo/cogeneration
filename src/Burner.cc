/** @file Burner.h
@brief Burner class implementation

This file is part of LIBPF
All rights reserved; do not distribute without permission.
@author (C) Copyright 2012-2024 Paolo Greppi simevo s.r.l.
*/

/* SYSTEM/GENERAL PURPOSE INCLUDES */
//
#include <list>
#include <string>

/* LIBPF INCLUDES */
//
#include <libpf/utility/diagnostic.h>
#include <libpf/streams/Stream.h>
#include <libpf/components/ListComponents.h> // for components and NCOMPONENTS
#include <libpf/utility/Error.h>

/* LOCAL INCLUDES */
//
#include <Burner.h>

/* FORWARD REFERENCES */
//

/* MACROS */
// avoid macros!!
//

/* CONSTANTS */
//

/* VARIABLES */
//

/* FUNCTIONS */
//

/*====================================================================================================================*/
/*==  Class Burner implementation  ===================================================================================*/
/*====================================================================================================================*/

std::string Burner::type_("Burner");

Libpf::Persistency::Defaults defaultsCombustor(Libpf::Persistency::Defaults &defaults) {
  std::list<std::string> reactions;
  if (components.exists("C3H8"))
    reactions.push_back("ReactionOxidationHydrocarbon<3,8>");
  if (components.exists("C4H10"))
    reactions.push_back("ReactionOxidationHydrocarbon<4,10>");
  if (components.exists("C5H12"))
    reactions.push_back("ReactionOxidationHydrocarbon<5,12>");
  if (components.exists("C6H14"))
    reactions.push_back("ReactionOxidationHydrocarbon<6,14>");

  if (components.exists("CH4"))
    reactions.push_back("ReactionOxidationCH4");
  if (components.exists("CO"))
    reactions.push_back("ReactionOxidationCO");
  if (components.exists("H2"))
    reactions.push_back("ReactionOxidationH2");
  if (components.exists("C2H6"))
    reactions.push_back("ReactionOxidationC2H6");
  if (components.exists("CH4O"))
    reactions.push_back("ReactionOxidationMeOH");
  if (components.exists("C2H4"))
    reactions.push_back("ReactionOxidationC2H4");
  if (components.exists("C6H6O"))
    reactions.push_back("ReactionOxidationPhenol");
  if (components.exists("C10H8"))
    reactions.push_back("ReactionOxidationNaphthalene");
  Libpf::Persistency::Defaults defaultsCombustor = defaults;
  defaultsCombustor("nReactions", reactions.size());
  int i(0);
  for (auto &reaction : reactions)
    defaultsCombustor(std::string("embeddedTypeReactions[").append(std::to_string(i++)).append("]"), reaction);
  return defaultsCombustor;
}

Burner::Burner(Libpf::Persistency::Defaults defaults, uint32_t id, Persistency *persistency, Persistent *parent, Persistent *root) :
  Model(defaults, id, persistency, parent, root),
  VertexBase(defaults, id, persistency, parent, root),
  FlashDrum(defaultsCombustor(defaults), id, persistency, parent, root) {
} // Burner::Burner

Value Burner::LHV(const Object &s) {
  static Value LHV_CH4(802618.0, "kJ/kmol");
  static Value LHV_C2H6(1428642.0, "kJ/kmol");
  static Value LHV_H2(241814.0, "kJ/kmol");
  static Value LHV_CO(282980.0, "kJ/kmol");

  Value lhv = Value(0.0, "W");
  if (components.exists("CH4"))
    lhv += s.Q("ndotcomps", "CH4") * LHV_CH4;
  if (components.exists("CO"))
    lhv += s.Q("ndotcomps", "CO") * LHV_CO;
  if (components.exists("H2"))
    lhv += s.Q("ndotcomps", "H2") * LHV_H2;
  if (components.exists("C2H6"))
    lhv += s.Q("ndotcomps", "C2H6") * LHV_C2H6;
  if (components.exists("CH4O"))
    throw ErrorRunTime(CURRENT_FUNCTION, "CH4O unsupported in LHV");
  if (components.exists("C2H4"))
    throw ErrorRunTime(CURRENT_FUNCTION, "C2H4 unsupported in LHV");
  if (components.exists("C6H6O"))
    throw ErrorRunTime(CURRENT_FUNCTION, "C6H6O unsupported in LHV");
  if (components.exists("C10H8"))
    throw ErrorRunTime(CURRENT_FUNCTION, "C10H8 unsupported in LHV");
  return lhv;
} // LHV
