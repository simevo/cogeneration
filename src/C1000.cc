/** @file C1000.cc
    @brief declaration/implementation of C1000 model class

    @attention This file is part of LIBPF
    @attention All rights reserved; do not distribute without permission.
    @author (C) Copyright 2009-2024 Paolo Greppi simevo s.r.l.
 */

/* SYSTEM INCLUDES */
//
#define _USE_MATH_DEFINES

/* PROJECT INCLUDES */
//
#include <libpf/utility/diagnostic.h>
#include <libpf/streams/Stream.h>
#include <libpf/components/ListComponents.h> // for components
#include <libpf/utility/utility.h> // for itoalpha

/* LOCAL INCLUDES */
//
#include "C1000.h"

/* LOCAL VARIABLES */
//
static const int verbosityFile = 0;

/* FUNCTIONS */
// 

/*====================================================================================================================*/
/*==  C1000 class implementation  ====================================================================================*/
/*====================================================================================================================*/

const int nUnits(4); // max 17 !

const std::string C1000::type_("C1000");

C1000::C1000(Libpf::Persistency::Defaults defaults, uint32_t id, Persistency *persistency, Persistent *parent, Persistent *root) :
  Model(defaults, id, persistency, parent, root),
  VertexBase(defaults, id, persistency, parent, root),
  FlowSheet(defaults, id, persistency, parent, root) {
  const int verbosityLocal = 3;

  diagnostic(2, "Entered with defaults = " << defaults);

  addVariable(LHV_CH4);
  addVariable(LHV_H2);
  addVariable(LHV_CO);
  addVariable(powerIn);
  addVariable(powerEl);

  if (!persistency) {
    diagnostic(2, "Define unit operations");
    for (int i=0; i < nUnits; ++i)
      addUnit("C200", defaults.relay(itoalpha(i), std::string("Unit").append(std::to_string(i))));
    addUnit("Divider", defaults.relay("SPLIT",  "Fuel divider") ("nOutlets", nUnits));
    addUnit("Mixer", defaults.relay("MIX", "Exhaust mixer"));

    diagnostic(2, "Define stream objects and connect");
    addStream("StreamVapor", defaults.relay("S02", "Total fuel feed"), "source", "out", "SPLIT", "in");
    for (int i=0; i < nUnits; ++i)
      addStream("StreamVapor", defaults.relay(std::string("S02").append(itoalpha(i)), "Fuel feed to first unit"), "SPLIT", "out", itoalpha(i), "fuel");
    for (int i=0; i < nUnits; ++i)
      addStream("StreamVapor", defaults.relay(std::string("S07").append(itoalpha(i)), "Exhaust from first unit"), itoalpha(i), "exhaust", "MIX", "in");
    addStream("StreamVapor", defaults.relay("S07", "Combined recuperator exhaust"), "MIX", "out", "sink", "in");
  }

  VertexBase::registerSynonym("fuel", "in1");
  VertexBase::registerSynonym("exhaust", "out1");
} // C1000::C1000

void C1000::setup(void) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, " entered for " << tag());

    diagnostic(3, "Calling flowsheet::setup to initialize any subflowsheet");
    FlowSheet::setup();

    diagnostic(3, "Setting input variables");
    // set up feed streams; if not standalone, these values will be overwritten 
    // feed natural gas to turbine
    Q("S02.T").set(288.15, "K");
    Q("S02.P").set(5.0, "bar");
    S("S02.flowoption") = "Mx";
    my_cast<Stream *>(&at("S02"), CURRENT_FUNCTION)->clearComposition();
    Value powerEl0(200.0 * static_cast<double>(nUnits), "kW"); // Total net electric output at the nominal point
    Value etaEl0(0.33); // Total net electric output at the nominal point
    Q("S02:Tphase.x", "CH4").set(0.98);
    Q("S02:Tphase.x", "CO").set(0.0);
    Q("S02:Tphase.x", "H2").set(0.0);
    Q("S02:Tphase.x", "N2").set(0.02);
    Q("S02:Tphase.mdot").set(components["CH4"]->MW()*powerEl0 / etaEl0 / Q("S02:Tphase.x", "CH4") / LHV_CH4);

    for (int i = 0; i < nUnits - 1; ++i) {
      Q("SPLIT.outSplit", i).set(one/static_cast<double>(nUnits));
    }

    diagnostic(3, "Making selected inputs visible in GUI");

    diagnostic(3, "Making selected outputs visible in GUI");
    powerIn.setOutput();
    powerEl.setOutput();
  } // try
  catch(Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // C1000::setup

void C1000::post(SolutionMode solutionMode, int level) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, " entered for " << tag());
    powerIn = Q("S02:Tphase.ndotcomps", "CH4") * LHV_CH4 +
      Q("S02:Tphase.ndotcomps", "H2") * LHV_H2 +
      Q("S02:Tphase.ndotcomps", "CO") * LHV_CO;
    powerEl = Value(0.0, "W");
    for (int i = 0; i < nUnits; ++i)
      powerEl += at(itoalpha(i)).Q("powerEl");
  } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // C1000::post
