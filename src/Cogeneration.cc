/** @file Cogeneration.cc
 * @brief Contains the implementation of the Cogeneration class
 *
 * @attention All rights reserved; do not distribute without permission.
 * @author (C) Copyright 2012-2024 Paolo Greppi simevo s.r.l.
 */

/* SYSTEM/GENERAL PURPOSE INCLUDES */
//

/* LIBPF INCLUDES */
//
#include <libpf/utility/diagnostic.h> // for diagnostic
#include <libpf/components/ListComponents.h> // for components and NCOMPONENTS
#include <libpf/streams/Stream.h>

/* LOCAL INCLUDES */
//
#include <Cogeneration.h>

/* FORWARD REFERENCES */
//

/* MACROS */
// avoid macros!!
//

/* CONSTANTS */
//

/* VARIABLES */
//
static const int verbosityFile = -1;

/* FUNCTIONS */
//

/*====================================================================================================================*/
/*==  Class Cogeneration implementation  =================================================================*/
/*====================================================================================================================*/

std::string Cogeneration::type_("Cogeneration");

Cogeneration::Cogeneration(Libpf::Persistency::Defaults defaults, uint32_t id, Persistency *persistency, Persistent *parent, Persistent *root) :
  Model(defaults, id, persistency, parent, root),
  VertexBase(defaults, id, persistency, parent, root),
  FlowSheet(defaults, id, persistency, parent, root) {
  static const int verbosityLocal = 0;
  try {
    addVariable(energyPriceTh);
    addVariable(Tfumes);
    addVariable(TwaterIn);
    addVariable(TwaterOut);
    addVariable(energyPriceEl);
    addVariable(powerIn);
    addVariable(We);
    addVariable(Wt);
    addVariable(W);
    addVariable(etaE);
    addVariable(etaT);
    addVariable(eta);
    addVariable(mdot);
    addVariable(turbine);

    turbine = retrieveString(defaults, id, persistency, "turbine", "C30");

    if (!persistency) {
      diagnostic(2, "Define unit operations");
      addUnit(turbine.value(), defaults.relay("Microturbine", "Microturbine unit"));
      addUnit("FlashDrum", defaults.relay("Cogenerator", "Cogeneration heat exchanger"));

      diagnostic(2, "Define stream objects and connect");
      addStream("StreamVapor", defaults.relay("S02", "Fuel"), "source", "out", "Microturbine", "fuel");
      addStream("StreamVapor", defaults.relay("S03", "Hot gases"), "Microturbine", "exhaust", "Cogenerator", "in");
      addStream("StreamVapor", defaults.relay("S04", "Fumes"), "Cogenerator", "out", "sink", "in");
    }
  } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
};  // Cogeneration::Cogeneration

void Cogeneration::post(SolutionMode solutionMode, int level) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, "Entered for " << fullTag());
    powerIn = Q("Microturbine.powerIn");    
    We = Q("Microturbine.powerEl");
    etaE = We / powerIn;
    energyPriceEl = energyPriceTh / etaE;
    Wt = Q("Cogenerator.duty");
    W = We + Wt;
    etaT = Wt / powerIn;
    eta = etaE + etaT;
    mdot = components["H2O"]->MW() * Wt / ((components["H2O"]->cpl(TwaterOut, P0) + components["H2O"]->cpl(TwaterIn, P0)) / 2.0) / (TwaterOut - TwaterIn);
    if (TwaterIn > Tfumes)
      setWarning("potential temperature crossing between specified TwaterIn and Tfumes");
    if (TwaterIn >= TwaterOut)
      setError("TwaterIn sould be lower than TwaterOut");
    diagnostic(3, "Done");
  } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // Cogeneration::post

void Cogeneration::setup(void) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, "Entered for " << fullTag());

    diagnostic(3, "Calling flowsheet::setup to initialize any subflowsheet");
    FlowSheet::setup();

    // set up feed streams; if not standalone, these values will be overwritten 

    // feed natural gas
    Q("S02.T").set(T0);
    Q("S02.P").set(10.0, "bar");
    S("S02.flowoption") = "Mx";
    my_cast<Stream *>(&at("S02"), CURRENT_FUNCTION)->clearComposition();
    Q("S02:Tphase.x", "CH4").set(0.98);
    Q("S02:Tphase.x", "CO").set(0.0);
    Q("S02:Tphase.x", "H2").set(0.0);
    Q("S02:Tphase.x", "N2").set(0.02);
    Value powerEl0; // Total net electric output at the nominal point
    Value etaEl0; // Total net electric output at the nominal point
    Value LHV_CH4(802618.0, "kJ/kmol");
    if (turbine.value() == "C30") {
      powerEl0 = Value(28.0, "kW");
      etaEl0 = Value(0.25);
    } else if (turbine.value() == "C65") {
      powerEl0 = Value(65.0, "kW");
      etaEl0 = Value(0.29);
    } else if (turbine.value() == "C200") {
      powerEl0 = Value(200.0, "kW");
      etaEl0 = Value(0.33);
    } else if (turbine.value() == "C1000") {
      powerEl0 = Value(1000.0, "kW");
      etaEl0 = Value(0.33);
    } else {
      throw ErrorRunTime(CURRENT_FUNCTION, "unsupported turbine model");
    }
    Q("S02:Tphase.mdot").set(components["CH4"]->MW()*powerEl0 / etaEl0 / Q("S02:Tphase.x", "CH4") / LHV_CH4);

    S("Cogenerator.option") = "PT";
    Q("Cogenerator.P").set(P0);
    Q("Cogenerator.T").set(80.0 + 273.15, "K");

    energyPriceTh.setInput();
    Tfumes.setInput();
    TwaterIn.setInput();
    TwaterOut.setInput();

    energyPriceEl.setOutput();
    powerIn.setOutput();
    We.setOutput();
    Wt.setOutput();
    W.setOutput();
    etaE.setOutput();
    etaT.setOutput();
    eta.setOutput();
    mdot.setOutput();
    diagnostic(3, "Done");
    } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // Cogeneration::setup

void Cogeneration::makeUserEquations(std::list<Assignment *>::iterator &p) {
  static const int verbosityLocal = -1;
  try {
    diagnostic(2, "Entered for " << tag());
    int i(0);
    MAKEASSIGNMENT(Q("Cogenerator.T"), Tfumes, "Set outlet temperature of the fumes");
  } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // Cogeneration::makeUserEquations
