/** @file MicroTurbineC30.cc
    @brief declaration/implementation of MicroTurbineC30 model class

    @attention This file is part of LIBPF
    @attention All rights reserved; do not distribute without permission.
    @author (C) Copyright 2009-2024 Paolo Greppi simevo s.r.l.
 */

/* SYSTEM INCLUDES */
//
#define _USE_MATH_DEFINES

/* PROJECT INCLUDES */
//
#include <libpf/utility/diagnostic.h>
#include <libpf/streams/Stream.h>

/* LOCAL INCLUDES */
//
#include "MicroTurbineC30.h"

/* LOCAL VARIABLES */
//
static const int verbosityFile = 0;

/* FUNCTIONS */
// 

/*====================================================================================================================*/
/*==  MicroTurbineC30 class implementation  ==========================================================================*/
/*====================================================================================================================*/

const std::string MicroTurbineC30::type_("MicroTurbineC30");

MicroTurbineC30::MicroTurbineC30(Libpf::Persistency::Defaults defaults, uint32_t id, Persistency *persistency, Persistent *parent, Persistent *root) :
  Model(defaults, id, persistency, parent, root),
  VertexBase(defaults, id, persistency, parent, root),
  FlowSheet(defaults, id, persistency, parent, root) {  
  addVariable(LHV_CH4);
  addVariable(LHV_H2);
  addVariable(LHV_CO);
  addVariable(powerIn);
  addVariable(power);
  addVariable(powerEl);
  addVariable(etaEl);
  addVariable(beta0);
  addVariable(mC0);
  addVariable(TC0);
  addVariable(PC0);
  addVariable(mrC0);
  addVariable(mT0);
  addVariable(PT0);
  addVariable(mrT0);
  addVariable(TIT);
  addVariable(mC);
  addVariable(mT);
  addVariable(maC);
  addVariable(maT);
  addVariable(nr);

  if (!persistency) {
    diagnostic(2, "Define unit operations");
    addUnit("Compressor", defaults.relay("T",  "Turbine"));
    addUnit("Compressor", defaults.relay("C",  "Compressor"));
    addUnit("Burner", defaults.relay("RX",  "Burner"));
    addUnit("Exchanger", defaults.relay("HX",  "Turbine recuperator"));

    diagnostic(2, "Define stream objects and connect");
    addStream("StreamVapor", defaults.relay("S01", "Air to GTS"), "source", "out", "C", "in");
    addStream("StreamVapor", defaults.relay("S02", "NG to GTS burner"), "source", "out", "RX", "in");
    addStream("StreamVapor", defaults.relay("S05", "Turbine Inlet"), "RX", "out", "T", "in");
    addStream("StreamVapor", defaults.relay("S06", "Turbine Outlet"), "T", "out", "HX", "hotin");
    addStream("StreamVapor", defaults.relay("S07", "Compressed Air and HX cold inlet"), "C", "out", "HX", "coldin");
    addStream("StreamVapor", defaults.relay("S09", "HX cold outlet and RX burner inlet"), "HX", "coldout", "RX", "in");
    addStream("StreamVapor", defaults.relay("S11", "Recuperator exhaust"), "HX", "hotout", "sink", "in");
  }
} // MicroTurbineC30::MicroTurbineC30

void MicroTurbineC30::setup(void) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, " entered for " << tag());

    double Pset = 4.0; // max pressure in bar

    diagnostic(3, "Calling flowsheet::setup to initialize any subflowsheet");
    FlowSheet::setup();

    diagnostic(3, "Setting input variables");
    beta0 = Value(3.94769306686405);
    mC0 = Value(0.306891411, "kg/s");
    TC0 = Value(288.15, "K");
    PC0 = Value(101325., "Pa");
    mT0 = Value(0.309236235, "kg/s");
    PT0 = Value(385000., "Pa");

    TIT = Value(1144.0, "K");

    mrC0 = mC0 * sqrt(TC0 / T0) / PC0;
    mrT0 = mT0 * sqrt(TIT / T0) / PT0;

    // set up feed streams; if not standalone, these values will be overwritten 
    // fresh air
    Q("S01.T").set(15.0 + 273.15, "K");
    Q("S01.P").set(1.0, "atm");
    S("S01.flowoption") = "Nx";
    my_cast<Stream *>(&at("S01"), CURRENT_FUNCTION)->clearComposition();
    Q("S01:Tphase.ndot").set(38.49159293, "kmol/h");
    Q("S01:Tphase.x", "N2").set(0.792);
    Q("S01:Tphase.x", "O2").set(0.198);
    Q("S01:Tphase.x", "H2O").set(0.01);

    // feed natural gas to turbine
    Q("S02.T").set(15.0 + 273.15, "K");
    Q("S02.P").set(Pset, "bar");
    S("S02.flowoption") = "Nx";
    my_cast<Stream *>(&at("S02"), CURRENT_FUNCTION)->clearComposition();
    Q("S02:Tphase.ndot").set(0.522280634, "kmol/h");
    Q("S02:Tphase.x", "CH4").set(0.98);
    Q("S02:Tphase.x", "CO").set(0.0);
    Q("S02:Tphase.x", "H2").set(0.0);
    Q("S02:Tphase.x", "N2").set(0.02);

    // Burner
    Q("RX.deltaP").set(100.0, "mbar");
    Q("RX.duty").set(0.0, "W");
    S("RX.option") = "DH";
    for (int i = 0; i < I("RX.nReactions"); ++i) {
      at("RX:reactions", i).Q("z").set(1.0);
      at("RX:reactions", i).Q("conv").setOutput();
      at("RX:reactions", i).Q("z").setOutput();
    }

    // Turbine recuperator
    Q("HX.deltaPhot").set(50.0, "mbar");
    S("HX.hotoption") = "D";
    Q("HX.deltaPcold").set(50.0, "mbar");
    S("HX.coldoption") = "D";
    // S("HX.option") = "Thot";
    // Q("HX.hotT").set(324.9144+273.15, "K");
    S("HX.option") = "UA";
    Q("HX.U").set(100.0, "W/(m2*K)");
    Q("HX.A").set(14.2866994561544, "m2");

    S("C.option") = "P";
    Q("C.P").set(Pset, "bar");
    Q("C.theta").set(0.85);
    Q("C.etaM").set(0.99);
    Q("C.etaE").set(1.0);

    S("T.option") = "P";
    Q("T.P").set(1013.25 + 50.0, "mbar");
    Q("T.theta").set(0.719017307023255);
    Q("T.etaM").set(0.99);
    Q("T.etaE").set(1.0);

    diagnostic(3, "Initializing cut streams");
    // Turbine recuperator (HX) cold outlet, streamB52.17
    Q("S09.T").set(540.2536 + 273.15, "K");
    Q("S09.P").set(Pset, "bar");
    S("S09.flowoption") = "Nx";
    my_cast<Stream *>(&at("S09"), CURRENT_FUNCTION)->clearComposition();
    Q("S09:Tphase.ndot").set(63.69577 / 2.0, "kmol/h");
    Q("S09:Tphase.x", "N2").set(0.79);
    Q("S09:Tphase.x", "O2").set(0.21);

    diagnostic(3, "Defining cut streams");
    FlowSheet::addCut("S09");

    diagnostic(3, "Making selected inputs visible in GUI");
    TIT.setInput();

    diagnostic(3, "Making selected outputs visible in GUI");
    powerIn.setOutput();
    powerEl.setOutput();
    etaEl.setOutput();
    mC.setOutput();
    mT.setOutput();
    maC.setOutput();
    maT.setOutput();
    nr.setOutput();

    Q("RX.T").setOutput();
    Q("RX.P").setOutput();
    Q("RX.duty").setOutput();
    Q("RX.deltaP").setOutput();
  } // try
  catch(Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // MicroTurbineC30::setup

void MicroTurbineC30::post(SolutionMode solutionMode, int level) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, " entered for " << tag());
    powerIn = Q("S02:Tphase.ndotcomps", "CH4") * LHV_CH4 +
              Q("S02:Tphase.ndotcomps", "H2") * LHV_H2 +
              Q("S02:Tphase.ndotcomps", "CO") * LHV_CO;
    powerEl = -Q("C.We") - Q("T.We");
    etaEl = powerEl / powerIn;
  } // try
  catch(Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // MicroTurbineC30::post

void MicroTurbineC30::makeUserEquations(std::list<Assignment *>::iterator &p) {
  static const int verbosityLocal = -1;
  try {
    diagnostic(2, " entered for " << tag());
    int i(0);

    diagnostic(3, "MicroTurbineC30 flowsheet specific equations");
    // turbine matching
    nr = sqrt((Q("C.cr") - One) / (beta0 - One));
    mC = mrC0 * nr * Q("S01.P") / sqrt(Q("S01.T") / T0);
    // mrT0 = mT0*sqrt(TIT/T0)/PT0;
    mT = mrT0 * nr * Q("S05.P") / sqrt(Q("S05.T") / T0);
    maC = Q("S01:Tphase.mdot");
    maT = Q("S05:Tphase.mdot");

    MAKEASSIGNMENT(Q("T.P"), Q("T.P") - (Q("S11.P") - Value(101325.0, "Pa")), "set discharge pressure");

    MAKEASSIGNMENT(Q("S01:Tphase.ndot"), mC / Q("S01:Tphase.AMW"),
                   "Adjust inlet air to balance GTS");
    MAKEASSIGNMENT(Q("S02:Tphase.ndot"), Q("S02:Tphase.ndot") +
                   (TIT - Q("S05.T")) * Q("S05:Tphase.cp") * Q("S05:Tphase.mdot") / LHV_CH4,
                   "Adjust NG to GTS burner to fix TIT");

    // linear
    // Value factor = One - (One - nr)/3.;
    // quadratic
    Value factor = One - (nr - One) * (nr - One) / 1.5;
    // cubic
    // Value factor = One + pow(nr-One, 3.)/0.27;
    MAKEASSIGNMENT(Q("C.theta"), 0.85 * factor, "Compressor efficiency");
    // MAKEASSIGNMENT(Q("T.theta"), 0.719017307023255*pow(factor, 0.1), "turbine efficiency");

    // // Enable these to match vendor data
    // MAKEASSIGNMENT(Q("HX.A"), Q("HX.A") * Value(275.+273.15, "K") / Q("S11.T"),
    //   "Match vendor claimed exhaust temperature");
    // MAKEASSIGNMENT(Q("T.theta"), Q("T.theta") * sqrt(Q("S05.T") / Value(1144., "K")),
    //   "Match vendor claimed TIT");
  } // try
  catch(Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // MicroTurbineC30::makeUserEquations
