/** @file DeltaH.cc
    @brief DeltaH class definition

    This file is part of LIBPF
    All rights reserved; do not distribute without permission.
    @author (C) Copyright 2012-2024 Paolo Greppi simevo s.r.l.
*/

/* SYSTEM/GENERAL PURPOSE INCLUDES */
//

/* LIBPF INCLUDES */
//
#include <libpf/utility/diagnostic.h>
#include <libpf/streams/Stream.h>
#include <libpf/components/ListComponents.h> // for components and NCOMPONENTS

/* LOCAL INCLUDES */
//
#include <DeltaH.h>

/* FORWARD REFERENCES */
//

/* MACROS */
// avoid macros!!
//

/* CONSTANTS */
//

/* VARIABLES */
//
static const int verbosityFile = 0;

/* FUNCTIONS */
//

/*====================================================================================================================*/
/*==  Class DeltaH implementation  ===================================================================================*/
/*====================================================================================================================*/

std::string DeltaH::type_("DeltaH");

DeltaH::DeltaH(Libpf::Persistency::Defaults defaults, uint32_t id, Persistency *persistency, Persistent *parent, Persistent *root) :
Model(defaults, id, persistency, parent, root),
VertexBase(defaults, id, persistency, parent, root),
FlowSheet(defaults, id, persistency, parent, root) {
  addVariable(lhv);
  addVariable(LHV);

  if (!persistency) {
    diagnostic(2, "Define unit operations");
    addUnit("Burner", defaults.relay("IdealCombustor", "Ideal Combustion Reactor"));

    diagnostic(2, "Define stream objects and connect");
    addStream("StreamVapor", defaults.relay("Fuel", "Fuel"), "source", "out", "IdealCombustor", "in");
    addStream("StreamVapor", defaults.relay("Oxydant", "Oxidant"), "source", "out", "IdealCombustor", "in");
    addStream("StreamVapor", defaults.relay("Flue", "Flue gases"), "IdealCombustor", "out", "sink", "in");
  }
} // DeltaH::DeltaH

void DeltaH::setup(void) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, " entered for " << tag());

    diagnostic(3, "Calling FlowSheet::setup to initialize any embedded FlowSheet");
    FlowSheet::setup();

    diagnostic(3, "Setting input variables");
    // fuel
    S("Fuel.flashoption") = "PT";
    S("Fuel.flowoption") = "Nx";
    my_cast<Stream *>(&at("Fuel"), CURRENT_FUNCTION)->clearComposition();
    Q("Fuel:Tphase.ndot").set(1.0, "kmol/s");
    if (components.exists("C2H6")) {
      Q("Fuel:Tphase.x", "CH4").set(0.886279381413047);
      Q("Fuel:Tphase.x", "C2H6").set(0.0733813934835132);
      Q("Fuel:Tphase.x", "CO2").set(0.00426476773644518);
      Q("Fuel:Tphase.x", "N2").set(0.036074457366995);
    }
    else {
      Q("Fuel:Tphase.x", "CH4").set(0.98);
      Q("Fuel:Tphase.x", "CO").set(0.0);
      Q("Fuel:Tphase.x", "H2").set(0.0);
      Q("Fuel:Tphase.x", "N2").set(0.02);
    }

    // oxidant
    Q("Oxydant.T").set(T0);
    Q("Oxydant.P").set(P0);
    S("Oxydant.flashoption") = "PT";
    S("Oxydant.flowoption") = "Nx";
    my_cast<Stream *>(&at("Oxydant"), CURRENT_FUNCTION)->clearComposition();
    Q("Oxydant:Tphase.ndot").set(100.0, "kmol/s");
    Q("Oxydant:Tphase.x", "O2").set(0.2);
    Q("Oxydant:Tphase.x", "N2").set(0.8);

    // combustor
    Q("IdealCombustor.deltaP").set(0.0, "mbar");
    Q("IdealCombustor.T").set(T0);
    S("IdealCombustor.option") = "DT";
    for (int i = 0; i < I("IdealCombustor.nReactions"); ++i)
      at("IdealCombustor:reactions", i).Q("z").set(1.0);
    lhv.setOutput();
  } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // DeltaH::setup

void DeltaH::post(SolutionMode solutionMode, int level) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, "Entered for " << tag());
    lhv = Q("IdealCombustor.duty") / Q("Fuel:Tphase.mdot");
    LHV = Q("IdealCombustor.duty") / Q("Fuel:Tphase.ndot");
    diagnostic(3, "Done");
  } // try
  catch (Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // DeltaH::post
