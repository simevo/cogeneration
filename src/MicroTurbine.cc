/** @file MicroTurbine.cc
    @brief declaration/implementation of MicroTurbine model class

    @attention This file is part of LIBPF
    @attention All rights reserved; do not distribute without permission.
    @author (C) Copyright 2009-2024 Paolo Greppi simevo s.r.l.
 */

/* SYSTEM INCLUDES */
//
#define _USE_MATH_DEFINES

/* PROJECT INCLUDES */
//
#include <libpf/utility/diagnostic.h>
#include <libpf/streams/Stream.h>
#include <libpf/components/ListComponents.h> // for components

/* LOCAL INCLUDES */
//
#include "MicroTurbine.h"

/* LOCAL VARIABLES */
//
static const int verbosityFile = 0;

/* FUNCTIONS */
// 

/*====================================================================================================================*/
/*==  MicroTurbine class implementation  =============================================================================*/
/*====================================================================================================================*/

const std::string MicroTurbine::type_("MicroTurbine");

MicroTurbine::MicroTurbine(Libpf::Persistency::Defaults defaults, uint32_t id, Persistency *persistency, Persistent *parent, Persistent *root) :
  Model(defaults, id, persistency, parent, root),
  VertexBase(defaults, id, persistency, parent, root),
  FlowSheet(defaults, id, persistency, parent, root),
  DEFINE(LHV_CH4, "methane Lower Heating Value", 802618.0, "kJ/kmol"),
  DEFINE(LHV_H2, "hydrogen Lower Heating Value", 241814.0, "kJ/kmol"),
  DEFINE(LHV_CO, "carbon monoxide Lower Heating Value", 282980.0, "kJ/kmol"),
  DEFINE(powerIn, "Lower Heating Value input", 0.0, "W"),
  DEFINE(powerEl, "Total net electric output", 0.0, "W"),
  DEFINE(etaEl, "Overall electric efficiency", 0.0, ""),
  DEFINE(beta0, "Compressor compression ratio at the nominal point", 3.6, ""),
  DEFINE(TC0, "Compressor inlet temperature at the nominal point", 296.15, "K"),
  DEFINE(PC0, "Compressor inlet pressure at the nominal point", 101325., "Pa"),
  DEFINE(PT0, "Turbine inlet pressure at the nominal point", 347676., "Pa"),
  DEFINE(mC0, "Compressor mass flow at the nominal point", 1.66667, "kg/s"),
  DEFINE(mT0, "Turbine mass flow at the nominal point", 1.87407, "kg/s"),
  DEFINE(TIT, "should-be Turbine Inlet Temperature", t0, "K"),
  DEFINE(Ctheta0, "Compressor isentropic yield at the nominal point", 0.8, ""),
  DEFINE(Ttheta0, "Turbine isentropic yield at the nominal point", 0.8, ""),
  DEFINE(n0, "Compressor and Turbine frequency at the nominal point", 0.0, "Hz"),
  DEFINE(n, "Compressor and Turbine frequency", 0.0, "Hz"),
  DEFINE(nr, "Compressor and Turbine reduced frequency", 0.0, ""),
  DEFINE(model, "Capstone Turbine model", "C30") {
    
  const int verbosityLocal = 3;

  diagnostic(2, "Entered with defaults = " << defaults);

  addVariable(LHV_CH4);
  addVariable(LHV_H2);
  addVariable(LHV_CO);
  addVariable(powerIn);
  addVariable(powerEl);
  addVariable(etaEl);
  addVariable(beta0);
  addVariable(TC0);
  addVariable(PC0);
  addVariable(PT0);
  addVariable(mC0);
  addVariable(mT0);
  addVariable(TIT);
  addVariable(Ctheta0);
  addVariable(Ttheta0);
  addVariable(n0);
  addVariable(n);
  addVariable(nr);
  addVariable(model);

  model = retrieveString(defaults, id, persistency, "model", "C30");

  if (!persistency) {
    diagnostic(2, "Define unit operations");
    addUnit("Compressor", defaults.relay("T",  "Turbine"));
    addUnit("Compressor", defaults.relay("C",  "Compressor"));
    addUnit("Burner", defaults.relay("RX",  "Burner"));
    addUnit("Exchanger", defaults.relay("HX",  "Turbine recuperator"));

    diagnostic(2, "Define stream objects and connect");
    addStream("StreamVapor", defaults.relay("S01", "Air feed"), "source", "out", "C", "in");
    addStream("StreamVapor", defaults.relay("S02", "Fuel feed"), "source", "out", "RX", "in");
    addStream("StreamVapor", defaults.relay("S03", "Turbine Inlet"), "RX", "out", "T", "in");
    addStream("StreamVapor", defaults.relay("S04", "Turbine Outlet"), "T", "out", "HX", "hotin");
    addStream("StreamVapor", defaults.relay("S05", "Compressed Air and HX cold inlet"), "C", "out", "HX", "coldin");
    addStream("StreamVapor", defaults.relay("S06", "HX cold outlet and RX burner inlet"), "HX", "coldout", "RX", "in");
    addStream("StreamVapor", defaults.relay("S07", "Recuperator exhaust"), "HX", "hotout", "sink", "in");
  }
  
  VertexBase::registerSynonym("air", "in1");
  VertexBase::registerSynonym("fuel", "in2");
  VertexBase::registerSynonym("exhaust", "out1");
} // MicroTurbine::MicroTurbine

void MicroTurbine::setup(void) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, " entered for " << tag());

    diagnostic(3, "Calling flowsheet::setup to initialize any subflowsheet");
    FlowSheet::setup();

    diagnostic(3, "Setting input variables");
    // iso ambient conditions
    TC0 = Value(288.15, "K");
    PC0 = Value(101325., "Pa");

    // set up feed streams; if not standalone, these values will be overwritten 
    // fresh air
    Q("S01.T").set(TC0);
    Q("S01.P").set(PC0);
    S("S01.flowoption") = "Mx";
    my_cast<Stream *>(&at("S01"), CURRENT_FUNCTION)->clearComposition();
    Q("S01:Tphase.x", "N2").set(0.792);
    Q("S01:Tphase.x", "O2").set(0.198);
    Q("S01:Tphase.x", "H2O").set(0.01);

    // feed natural gas to turbine
    Q("S02.T").set(TC0);
    S("S02.flowoption") = "Mx";
    my_cast<Stream *>(&at("S02"), CURRENT_FUNCTION)->clearComposition();
    Q("S02:Tphase.x", "CH4").set(0.98);
    Q("S02:Tphase.x", "CO").set(0.0);
    Q("S02:Tphase.x", "H2").set(0.0);
    Q("S02:Tphase.x", "N2").set(0.02);

    // Burner
    Q("RX.duty").set(0.0, "W");
    S("RX.option") = "DH";
    for (int i = 0; i < I("RX.nReactions"); ++i) {
      at("RX:reactions", i).Q("z").set(1.0);
      at("RX:reactions", i).Q("conv").setOutput();
      at("RX:reactions", i).Q("z").setOutput();
    }

    // Turbine recuperator
    S("HX.hotoption") = "D";
    S("HX.coldoption") = "D";
    // S("HX.option") = "Thot";
    // Q("HX.hotT").set(324.9144+273.15, "K");
    S("HX.option") = "UA";

    S("C.option") = "P";
    Q("C.etaM").set(0.99);
    Q("C.etaE").set(1.0);

    S("T.option") = "P";
    Q("T.P").set(1013.25 + 50.0, "mbar");
    Q("T.etaM").set(0.99);
    Q("T.etaE").set(1.0);

    diagnostic(3, "Initializing cut streams");
    // Turbine recuperator (HX) cold outlet, streamB52.17
    Q("S06.T").set(540.2536 + 273.15, "K");
    S("S06.flowoption") = "Mx";
    my_cast<Stream *>(&at("S06"), CURRENT_FUNCTION)->clearComposition();
    Q("S06:Tphase.x", "N2").set(0.79);
    Q("S06:Tphase.x", "O2").set(0.21);

    diagnostic(3, "Initializing model-dependent variables");
    if (model.value() == "C30") {
      // data from:
      // 
      // Capstone Turbine Corporation
      // Final Technical Report DOE Project ID # DE-FC26-00CH11058: Advanced MicroTurbine System (AMTS)
      // C200 MicroTurbine - Ultra-Low Emissions MicroTurbine
      // March 31, 2008
      // http://dx.doi.org/10.2172/975026 
      //
      // and from:
      // Product Specification Model C30 - Capstone MicroTurbine
      // 460000 Rev J (December 2010)
      beta0.set(3.64);
      n0 = Value(96000, "rpm");
      TIT = Value(1097.5944444445, "K"); // should-be Turbine Inlet Temperature
      Value powerEl0(28.0, "kW"); // Total net electric output at the nominal point
      Value etaEl0(0.25); // Total net electric output at the nominal point
      Q("S02:Tphase.mdot").set(components["CH4"]->MW()*powerEl0 / etaEl0 / Q("S02:Tphase.x", "CH4") / LHV_CH4); // fuel to burner
      mT0 = Value(0.312978756, "kg/s"); // Turbine mass flow at the nominal point
      Ctheta0 = Value(0.805);
      Ttheta0 = Value(0.858);
      Q("RX.deltaP").set(92.0, "mbar");
      Q("HX.deltaPhot").set(180.0, "mbar");
      Q("HX.deltaPcold").set(180.0, "mbar");
      Q("HX.U").set(100.0, "W/(m2*K)");
      Q("HX.A").set(14.2866994561544, "m2");
    } else if (model.value() == "C65") {
      // data from:
      //
      // Product Specification, C65, All Packages
      // 460044 Rev D (May 2013)
      beta0.set(4.8);
      n0 = Value(96000, "rpm");
      TIT = Value(1203.15, "K"); // should-be Turbine Inlet Temperature
      Value powerEl0(65.0, "kW"); // Total net electric output at the nominal point
      Value etaEl0(0.29); // Total net electric output at the nominal point
      Q("S02:Tphase.mdot").set(components["CH4"]->MW()*powerEl0 / etaEl0 / Q("S02:Tphase.x", "CH4") / LHV_CH4); // fuel to burner
      mT0 = Value(0.5214124855, "kg/s"); // Turbine mass flow at the nominal point
      Ctheta0 = Value(0.777);
      Ttheta0 = Value(0.86);
      Q("RX.deltaP").set(92.0, "mbar");
      Q("HX.deltaPhot").set(180.0, "mbar");
      Q("HX.deltaPcold").set(180.0, "mbar");
      Q("HX.U").set(100.0, "W/(m2*K)");
      Q("HX.A").set(22.0, "m2");
    } else if (model.value() == "C200") {
      // data from:
      //
      // Product Specification, Model C200 - Capstone MicroTurbine
      // 460045 Rev D (July 2009)
      beta0.set(4.8);
      n0 = Value(96000, "rpm");
      TIT = Value(1203.15, "K"); // should-be Turbine Inlet Temperature
      Value powerEl0(200.0, "kW"); // Total net electric output at the nominal point
      Value etaEl0(0.33); // Total net electric output at the nominal point
      Q("S02:Tphase.mdot").set(components["CH4"]->MW()*powerEl0 / etaEl0 / Q("S02:Tphase.x", "CH4") / LHV_CH4); // fuel to burner
      mT0 = Value(1.6043461092, "kg/s"); // Turbine mass flow at the nominal point
      Ctheta0 = Value(0.767);
      Ttheta0 = Value(0.84);
      Q("RX.deltaP").set(90.0, "mmH2O");
      Q("HX.deltaPhot").set(180.0, "mmH2O");
      Q("HX.deltaPcold").set(180.0, "mmH2O");
      Q("HX.U").set(100.0, "W/(m2*K)");
      Q("HX.A").set(50.0, "m2");
    } else {
      throw ErrorRunTime(CURRENT_FUNCTION, "unsupported model");
    }
    mC0 = mT0 - Q("S02:Tphase.mdot"); // Compressor mass flow at the nominal point
    Q("S01:Tphase.mdot").set(mC0); // Air feed
    PT0 = PC0 * beta0 - Q("RX.deltaP") - Q("HX.deltaPcold");
    Q("S06:Tphase.mdot") = Q("S01:Tphase.mdot"); // HX cold outlet and RX burner inlet
    Q("C.theta") = Ctheta0;
    Q("T.theta") = Ttheta0;
    Q("S02.P").set(PC0 * beta0);
    Q("S06.P").set(PC0 * beta0);
    Q("C.P").set(PC0 * beta0);

    diagnostic(3, "Defining cut streams");
    FlowSheet::addCut("S06");

    diagnostic(3, "Making selected inputs visible in GUI");
    TIT.setInput();

    diagnostic(3, "Making selected outputs visible in GUI");
    powerIn.setOutput();
    powerEl.setOutput();
    etaEl.setOutput();
    n0.setOutput();
    n.setOutput();
    nr.setOutput();

    Q("RX.T").setOutput();
    Q("RX.P").setOutput();
    Q("RX.duty").setOutput();
    Q("RX.deltaP").setOutput();
  } // try
  catch(Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // MicroTurbine::setup

void MicroTurbine::post(SolutionMode solutionMode, int level) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, " entered for " << tag());
    powerIn = Q("S02:Tphase.ndotcomps", "CH4") * LHV_CH4 +
              Q("S02:Tphase.ndotcomps", "H2") * LHV_H2 +
              Q("S02:Tphase.ndotcomps", "CO") * LHV_CO;
    powerEl = -Q("C.We") - Q("T.We");
    etaEl = powerEl / powerIn;
  } // try
  catch(Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // MicroTurbine::post

void MicroTurbine::makeUserEquations(std::list<Assignment *>::iterator &p) {
  static const int verbosityLocal = -1;
  try {
    diagnostic(2, " entered for " << tag());
    int i(0);

    diagnostic(3, "MicroTurbine flowsheet specific equations");
    nr = sqrt((Q("C.cr") - One) / (beta0 - One));
    n = n0 * nr;
    MAKEASSIGNMENT(Q("T.P"), Q("T.P") - (Q("S07.P") - Value(101325.0, "Pa")), "set discharge pressure");

    // use one of the two:
    // MAKEASSIGNMENT(Q("S02:Tphase.mdot"), Q("S02:Tphase.mdot") +
    //                 (TIT - Q("S03.T")) * Q("S03:Tphase.Cp") * Q("S03:Tphase.mdot") / LHV_CH4,
    //                 "Adjust fuel to burner to fix TIT");
    MAKEASSIGNMENT(Q("S01:Tphase.mdot"), Q("S01:Tphase.mdot") * Q("S03.T") / TIT, "Adjust air to fix TIT");

    // MAKEASSIGNMENT(Q("S02.P"), Q("S01.P") * beta0, "set fuel pressure");
    MAKEASSIGNMENT(Q("C.P"), Q("S01.P") * beta0, "set discharge pressure");

    // linear
    // Value factor = One - (One - nr)/3.;
    // quadratic
    Value factor = One - (nr - One) * (nr - One) / 1.5;
    // cubic
    // Value factor = One + pow(nr-One, 3.)/0.27;

    // // Enable these to vary the efficiency with the load
    MAKEASSIGNMENT(Q("C.theta"), Ctheta0 * factor, "Compressor efficiency");
    MAKEASSIGNMENT(Q("T.theta"), Ttheta0 * pow(factor, 0.1), "turbine efficiency");

    // // Enable these to match vendor data
    // MAKEASSIGNMENT(Q("HX.A"), Q("HX.A") * Value(275.+273.15, "K") / Q("S07.T"),
    //   "Match vendor claimed exhaust temperature");
    // MAKEASSIGNMENT(Q("T.theta"), Q("T.theta") * sqrt(Q("S03.T") / Value(1144., "K")),
    //   "Match vendor claimed TIT");
  } // try
  catch(Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // MicroTurbine::makeUserEquations
